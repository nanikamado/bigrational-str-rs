#[cfg(test)]
mod tests;

use num::{bigint::ToBigInt, rational::Ratio, traits::Pow};
use num::{BigInt, BigRational, One, Signed, Zero};
use regex::Regex;
use std::collections::HashMap;
use std::fmt::Write;
use std::str::FromStr;

lazy_static::lazy_static! {
    static ref REGEX: Regex =
        Regex::new(r"^(?P<neg>-)?(?P<int>[[:alnum:]]+)(\.(?P<frac>[[:alnum:]]*)(\((?P<repeat>[[:alnum:]]+)\))?)?$")
        .unwrap();
}

pub fn big_rational_to_string(mut n: BigRational) -> String {
    let neg = if n.is_negative() {
        n = -n;
        true
    } else {
        false
    };
    let mut fraction = String::new();
    let mut map = HashMap::new();
    let mut rem = n.numer() % n.denom();
    while !rem.is_zero() && !map.contains_key(&rem) {
        map.insert(rem.clone(), fraction.len());
        rem *= 10;
        fraction.push_str(&(rem.clone() / n.denom()).to_string());
        rem %= n.denom();
    }
    let mut output = if neg { "-".to_owned() } else { String::new() };
    output.push_str(&(n.numer() / n.denom()).to_string());
    if rem.is_zero() {
        if !fraction.is_empty() {
            write!(output, ".{}", &fraction).unwrap();
        }
    } else {
        write!(
            output,
            ".{}({})",
            &fraction[..map[&rem]],
            &fraction[map[&rem]..]
        )
        .unwrap();
    }
    output
}

pub fn big_rational_to_str_radix(mut n: BigRational, radix: u32) -> String {
    let neg = if n.is_negative() {
        n = -n;
        true
    } else {
        false
    };
    let mut fraction = String::new();
    let mut map = HashMap::new();
    let mut rem = n.numer() % n.denom();
    while !rem.is_zero() && !map.contains_key(&rem) {
        map.insert(rem.clone(), fraction.len());
        rem *= radix;
        fraction.push_str(&(rem.clone() / n.denom()).to_str_radix(radix));
        rem %= n.denom();
    }
    let mut output = if neg { "-".to_owned() } else { String::new() };
    output.push_str(&(n.numer() / n.denom()).to_str_radix(radix));
    if rem.is_zero() {
        if !fraction.is_empty() {
            write!(output, ".{}", &fraction).unwrap();
        }
    } else {
        write!(
            output,
            ".{}({})",
            &fraction[..map[&rem]],
            &fraction[map[&rem]..]
        )
        .unwrap();
    }
    output
}

pub fn str_to_big_rational(string: &str) -> Result<BigRational, ()> {
    str_to_big_rational_radix(string, 10).ok_or(())
}

pub fn str_to_big_rational_radix(string: &str, radix: u32) -> Option<BigRational> {
    let captures = REGEX.captures(string)?;
    let sign = if captures.name("neg").is_some() {
        -1
    } else {
        1
    }
    .to_bigint()
    .unwrap();
    let int = &captures["int"];
    let fraction = captures.name("frac").map(|a| a.as_str());
    let repeating = captures.name("repeat").map(|a| a.as_str());

    match fraction {
        None => Some(Ratio::from_integer(BigInt::parse_bytes(int.as_bytes(), radix)?) * sign),
        Some(fraction) => match repeating {
            None => frac(sign, int, fraction, radix),
            Some(repeating) => repeat(sign, int, fraction, repeating, radix),
        },
    }
}

fn read(s: &str, radix: u32) -> Option<BigRational> {
    Some(Ratio::from_integer(BigInt::parse_bytes(
        s.as_bytes(),
        radix,
    )?))
}

fn frac(sign: BigInt, integer: &str, fractional: &str, radix: u32) -> Option<BigRational> {
    let mut a = BigInt::one();
    for _ in 0..fractional.len() {
        a *= radix;
    }
    let b = BigRational::new(BigInt::from_str(fractional).unwrap(), a) + read(integer, radix)?;
    Some(b * sign)
}

fn repeat(
    sign: BigInt,
    integer: &str,
    fractional: &str,
    repeating: &str,
    radix: u32,
) -> Option<BigRational> {
    let a: BigInt = Pow::pow(radix, fractional.len()).to_bigint().unwrap();
    let b = read(fractional, radix).unwrap_or_else(BigRational::zero) / &a;
    let mut c = (radix - 1).to_bigint().unwrap();
    for _ in 1..repeating.len() {
        c *= radix;
        c += radix - 1;
    }
    let d = read(repeating, radix).unwrap() / c / a;
    Some((b + d + read(integer, radix).unwrap()) * sign)
}
