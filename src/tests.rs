use crate::*;
use num::BigRational;
use std::str::FromStr;

#[test]
fn test0() {
    assert_eq!(
        "-0.1(6)",
        big_rational_to_string(BigRational::from_str("1/-6").unwrap())
    )
}

#[test]
fn test1() {
    assert_eq!(
        "-58/55",
        str_to_big_rational("-1.0(54)").unwrap().to_string()
    )
}

#[test]
fn test2() {
    let s = big_rational_to_string(BigRational::from_str("99989/99991").unwrap());
    assert_eq!("99989/99991", str_to_big_rational(&s).unwrap().to_string())
}

#[test]
fn test3() {
    assert_eq!(
        "0.0(0011)",
        big_rational_to_str_radix(BigRational::from_str("1/10").unwrap(), 2)
    )
}

#[test]
fn test4() {
    assert_eq!(
        "0.3(h)",
        big_rational_to_str_radix(BigRational::from_str("1/10").unwrap(), 35)
    )
}

#[test]
fn test5() {
    assert_eq!(
        "0.(770992366412213740458015267175572519083969465648854961832061068702\
            2900763358778625954198473282442748091603053435114503816793893129)",
        big_rational_to_string(BigRational::from_str("101/131").unwrap())
    )
}

#[test]
fn test6() {
    assert_eq!(
        "0.(jieu88skvvlpk2757ywfe0jsazq3ui4y2qxiymj8ioqdqnmt4o6lfnwpa61ncwz6b)",
        big_rational_to_str_radix(BigRational::from_str("71/131").unwrap(), 36)
    )
}

#[test]
fn test7() {
    assert_eq!(
        BigRational::from_str("71/131").unwrap(),
        str_to_big_rational_radix("0.(jieu88skvvlpk2757ywfe0jsazq3ui4y2qxiymj8ioqdqnmt4o6lfnwpa61ncwz6b)", 36).unwrap()
    )
}

#[test]
fn test8() {
    assert_eq!(
        BigRational::from_str("5/6").unwrap(),
        str_to_big_rational_radix("0.(A)", 13).unwrap()
    )
}