# big-rational-str

Crate for formatting and parsing BigRationals (of the popular 'num' crate) in decimal form.

### Converts fractions to decimal strings and vice versa
|BigRational    |String             |
|---            |---                |
|`1/3`          |`"0.(3)"`          |
|`-93/52`       |`"-1.78(846153)"`  |
|`30/4`         |`"7.5"`            |
|`550/-1`       |`"-550"`           |

#### Usage
```rust
use big_rational_str::*;

fn main() {
    let big_rational = str_to_big_rational("3.(3)").unwrap();
    println!("{}", big_rational_to_string(big_rational));
}
```
